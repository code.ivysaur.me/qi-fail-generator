# qi-fail-generator

![](https://img.shields.io/badge/written%20in-Javascript-blue)

A joke website to generate failure messages in the style of the TV show "QI".


## Download

- [⬇️ qi-fail-generator_co_uk.zip](dist-archive/qi-fail-generator_co_uk.zip) *(79.44 KiB)*
